var server = require('./lib/mipsock');
var async = require('async');
var handleFunc = require('./lib/miphandler');
var debwrap = require('./lib/debugwrap');
var debug = debwrap.debug('mip:sock:index:deb');
var er = debwrap.error('mip:sock:index:err');

var mip2opc = require("./lib/mipselect");
var globals = require("./lib/globals.js");

async.series([

    function (callback) {
      server.addHandlerForKeyword('opc_read', handleFunc.opcRead,
        function (err) {
          if (err) {
            callback(err);
          } else {
            callback();
          }
        });
    },
    
    function (callback) {
      server.addHandlerForKeyword('opc_write', handleFunc.opcWrite,
        function (err) {
          if (err) {
            callback(err);
          } else {
            callback();
          }
        });
    }
    
  ],
  function (err) {
    if (err) {
      er(err);
    } else {
      server.startServer(function () {
        console.log("MIP-2-OPC-Client started.");
        
        mip2opc.select('get server namespace settings ...',function(error,result){
            if (error)
                console.log('get server namespace settings ... failure');
            else if (result){
                for (let entry of result.data.data){
                    globals.addNodeItem(entry);
                }
                console.log('get server namespace settings ... done');
            }
        });
        
      });
    }
  }
);

process.on('SIGINT',function(){
    console.log("MIP-2-OPC-Client shutdown requested.");
    console.log("MIP-2-OPC-Client shutdown completed.");
    process.exit(0);
});

