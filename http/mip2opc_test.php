<?php

/** --------------------------------------------------
 *  +++++ Test der MIP2OPC Command-Schnittstelle +++++
 *  --------------------------------------------------
 *       1) newmat --> m2o_SendRequestNewMat
 *       2) opc_read --> m2o_SendRequestOpcRead
 *       3) opc_browse --> m2o_SendRequestOpcBrowse
 *  --------------------------------------------------
 *  mip2opc läuft auf M2O_HOST:M2O_PORT
 *  --------------------------------------------------
 */
    const M2O_PORT = 20003;
    const M2O_HOST = 'localhost';
    
    require_once 'mip2opc_config.php';
    
    error_reporting(E_ALL);
    
    $socket = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
    if ($socket) {
        $result = socket_connect($socket,M2O_HOST,M2O_PORT);
        if ($result) {
            
            switch($argv[1]) {
                
                case 'newmat' :
                    $done = m2o_SendRequestNewMat($socket);
                    $done = m2o_GetResponse($socket,'newmat');
                    break;
            
                case 'opc_read' :
                    $done = m2o_SendRequestOpcRead($socket);
                    $done = m2o_GetResponse($socket,'opc_read');
                    break;
            
                case 'opc_browse' :
                    $done = m2o_SendRequestOpcBrowse($socket);
                    $done = m2o_GetResponse($socket,'opc_browse');
                    break;
            }
            
            socket_close($socket);
        }
        else {
            $error = socket_strerror(socket_last_error($socket));
            echo 'error(socket_connect): '.$error."\n";
        }
    }
    else {
        $error = socket_strerror(socket_last_error($socket));
        echo 'error(socket_create): '.$error."\n";
    }
    
    function m2o_SendRequestNewMat($socket) {
        $request = [
            'header'  => [
                'datetime' => time(),
                'nodeid'   => 0,
                'serialnr' => 0
            ],
            'request' => 'newmat',
            'data'    => [
                'rawmatid' => 991,
                'silolnr'  => 3
            ]
        ];
        $request2Json = json_encode($request);
        $done = socket_write($socket,$request2Json,strlen($request2Json));
        
        echo 'm2o_SendRequest(newmat): '.$request2Json."\n";
        return $done;
    }

    function m2o_SendRequestOpcBrowse($socket) {
        $request = [
            'header'  => [
                'datetime' => time(),
                'nodeid'   => 0,
                'serialnr' => 0
            ],
            'request' => 'opc_browse',
            'data'    => [
                'folder' => 'RootFolder'
            ]
        ];
        $request2Json = json_encode($request);
        $done = socket_write($socket,$request2Json,strlen($request2Json));
        
        echo 'm2o_SendRequest(opc_browse): '.$request2Json."\n";
        return $done;
    }

    function m2o_SendRequestOpcRead($socket) {
        $request = [
            'header'  => [
                'datetime' => time(),
                'nodeid'   => 0,
                'serialnr' => 0
            ],
            'request' => 'opc_read',
            'data'    => [ 
                'node' => getNodeArray()
            ]
        ];
        $request2Json = json_encode($request);
        $done = socket_write($socket,$request2Json,strlen($request2Json));
        
        echo 'm2o_SendRequest(opc_read): '.$request2Json."\n";
        return $done;
    }

    function m2o_GetResponse($socket,$command) {
        $response2Json = socket_read($socket,1024);
        $response = json_decode($response2Json);
        echo 'm2o_GetResponse('.$command.'): '.$response2Json."\n";
        echo 'response: '.print_r($response,true);
        return 0;
    }

?>
