<?php

    $M2O_CLIENT_REQUEST = [
        "php mip2opc_test.php newmat",
        "php mip2opc_test.php opc_read",
        "php mip2opc_test.php opc_browse" ];
    
    foreach ($M2O_CLIENT_REQUEST as $key => $request) {
        $btnId = 'id_opc_button_'.$key;
        if(array_key_exists($btnId, $_POST))
        $M2O_SERVER_RESPONSE = getServerResponse($request);
    }

    function getServerResponse($request) { 
        $pipes = [];
        $response = [];
        $stdIO = [ 
            0 => [ 'pipe', 'r' ],
            1 => [ 'pipe', 'w' ],
            2 => [ 'pipe', 'w' ]
        ];
        $process = proc_open ($request,$stdIO,$pipes);
        if (is_resource($process)) {
            
            fclose($pipes[0]);
            
            while ( !feof($pipes[1]) )
            $response[] = 'stdout: '.fgets($pipes[1]);
            fclose($pipes[1]);
            
            while ( !feof($pipes[2]) )
            $response[] = 'stderr: '.fgets($pipes[2]);
            fclose($pipes[2]);
            
            $status = proc_get_status($process);
            $response[] = 'exitcode: '.strval($status['exitcode']);
            
            proc_close($process);
        }
     // return implode('',$response);
        return implode('<br/>',$response);
    }
    
?>

<table border="1" style="width:80%" align="center">
    
  <thead>
    <th style="width:50%" align="center">Client Request (MIP2OPC)</th>
    <th style="width:50%" align="center">Server Response (MIP2OPC)</th>
  </thead>
  
  <tbody>
    <td align="center">
      <form method="post">
        <table style="width:80%" align="center">
            
    <?php
    foreach ($M2O_CLIENT_REQUEST as $key => $request) {
        $btnId = 'id_opc_button_'.$key;
    ?>
          <tr>
            <td align="left"><button type="button" disabled><?php echo $request;?></button></td>
            <td align="right"><input type="submit" name="<?php echo $btnId;?>" value="submit"></td>
          </tr>
          
    <?php } ?>
          
        </table>
      </form>
    </td>
    <td>
      <div>
      <?php echo "$M2O_SERVER_RESPONSE";?>
      </div>
    </td>
  </tbody>
  
</table>

