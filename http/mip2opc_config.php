<?php

    const M2O_CONF = 
    array (
        
      array (
        'line'      => 'M32',
        'component' => 'Ex',
        'variable'  => array (
               9 => 'Lstg_kg_h',
              12 => 'Meter_gr_PV',
               6 => 'Meter_min_PV',
              11 => 'Meter_gr_SP' )  // forumplast
      )

    );
    
    function getNodeArray(){
        $nodeArray = [];
        foreach (M2O_CONF as $next) {
            
            $variable  = $next['variable'];
            $prefix    = 'ns=4;s=Address Space.';
            $prefix   .= $next['line'].'.';
            $prefix   .= $next['component'].'.';
            foreach ($variable as $key => $value) {
                $nodeArray[] = $prefix.$value;
            }
        }
        return $nodeArray;
    }

?>
