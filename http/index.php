<!DOCTYPE html>
<html lang="de">
<head>

    <title>Test MIP-2-OPC-Interface</title>
    <link rel="stylesheet" href="../../dojo-release-1.14.2/dijit/themes/claro/claro.css">

    <script>dojoConfig = {async: true, parseOnLoad: true}</script>
    <script src='../../dojo-release-1.14.2/dojo/dojo.js'></script>

</head>

<body class="claro">
    <?php require_once 'mip2opc.php'; ?>
</body>

</html>
