<?php

class Mip2Opc {
    
    const M2O_PORT = 20003;
    const M2O_HOST = 'localhost';
    
    private $socket = null;
    private $nodeArray = null;
    
    public function __construct($filename){
    
        error_reporting(E_ALL);

        $socket = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
        if ($socket) {
            $result = socket_connect($socket,self::M2O_HOST,self::M2O_PORT);
            if ($result) {
                $this->socket = $socket;
                $this->nodeArray = $this->getNodeArray($filename);
            }
            else {
                $error = socket_strerror(socket_last_error($socket));
                echo 'error(socket_connect): '.$error."\n";
            }
        }
        else {
            $error = socket_strerror(socket_last_error($socket));
            echo 'error(socket_create): '.$error."\n";
        }
    }
    
    public function sendRequest() {
        $request = [
            'header'  => [
                'datetime' => time(),
                'nodeid'   => 0,
                'serialnr' => 0
            ],
            'request' => 'opc_read',
            'data'    => [ 
                'node' => $this->nodeArray
            ]
        ];
        $request2Json = json_encode($request);
        $done = socket_write($this->socket,$request2Json,strlen($request2Json));
        
        echo 'sendRequest(opc_read): '.$request2Json."\n";
        return $done;
    }

    public function getResponse() {
        $response2Json = socket_read($this->socket,4096);
        $response = (array)json_decode($response2Json);
        echo 'getResponse(opc_read): '.$response2Json."\n";
        return $response;
    }
    
    private function getNodeArray($filename){
        return 
            strpos($filename,'forumplast') ?
                $this->getNodeArrayFP($filename) : 
                $this->getNodeArrayRB($filename);
    }

    private function getNodeArrayFP($filename){
        $tree = include $filename;
        $nodeArray = [];
        foreach ($tree as $next) {
            $variable  = $next['variable'];
            $prefix    = 'ns=4;s=Address Space.';
            $prefix   .= $next['line'].'.';
            $prefix   .= $next['component'].'.';
            foreach ($variable as $key => $value) {
                $nodeArray[] = $prefix.$value;
            }
        }
        return $nodeArray;
    }

    private function getNodeArrayRB($filename){
        $tree = include $filename;
        $nodeArray = [];
        foreach ($tree as $next) {
            $variable  = $next['variable'];
            $prefix    = 'ns=1;';
            foreach ($variable as $key => $value) {
                $nodeArray[] = $prefix.$value;
            }
        }
        return $nodeArray;
    }

}

?>
