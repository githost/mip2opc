# Makefile for opcMIP

install:
	npm install
	make pretest
	cp ./git_hooks/config.js.sample ./lib/config.js

test:
	@./node_modules/.bin/mocha -R spec tests

pretest:
	@node git_hooks/link.js

clean:
	@rm -rf node_modules
	@rm .git/hooks/*
	@rm coverage.html
	@rm lib/config.js

coverage:
	./node_modules/.bin/jscoverage --no-highlight lib lib-cov
	LIB_COV=1 ./node_modules/.bin/mocha -R html-cov tests > coverage.html
	rm -rf lib-cov

.PHONY: test
