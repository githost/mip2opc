/*global require,setInterval,console */
const opcua = require("node-opcua");
const m2o = require("./lib/mipselect");
const config = require("./lib/config").request;

// Let's create an instance of OPCUAServer
const server = new opcua.OPCUAServer({
    port: config.opcPort, // the port of the listening socket of the server
    resourcePath: config.opcPath, // this path will be added to the endpoint resource name
     buildInfo : {
        productName: "MIP-2-OPC-Server",
        buildNumber: "4711",
        buildDate: new Date(2020,20,2)
    }
});

function post_initialize() {
    console.log("initialized");
    function construct_my_address_space(server) {
    
        // add some variables
        // add a variable named MyVariable1 to the newly created folder "MyDevice"
        let variable = 1;
        
        // emulate variable1 changing every 500 ms
        setInterval(function(){
            variable = Math.floor(Math.random()*Math.floor(100)); 
        }, 500);
        
        const addressSpace = server.engine.addressSpace;
        const namespace = addressSpace.getOwnNamespace();
    
        // declare a new object
        const extruder = namespace.addObject({
            organizedBy: addressSpace.rootFolder.objects,
            browseName: "Extruder"
        });
        
        function addDevice(namespace,extruder,line){
            return namespace.addObject({
                organizedBy: extruder,
                browseName: line
            });
        }
        
        function addComponent(namespace,device,component){
            return namespace.addObject({
                organizedBy: device,
                browseName: component
            });
        }
        
        function addNextNode(namespace,parent,entry){
            
            var nodeId   = 's=Address Space.Extruder.';
            var dataType = 
                entry.length > 4 ? 
                entry[4].toLowerCase() : 'default';
            
            switch (dataType){
                case 'integer' : dataType = 'Int32';  break;
                case 'short'   : dataType = 'Int16';  break;
                case 'float'   : dataType = 'Float';  break;
                case 'string'  : dataType = 'String'; break;
                default        : dataType = 'Double'; break;
            }
            
            nodeId += entry.slice(0,3).join('.');
            namespace.addVariable({
                componentOf: parent,
                nodeId: nodeId,
                browseName: entry[2],
                dataType: dataType,
                valueRank: 0,
                arrayDimension: []
            });
        }
    
        m2o.select('initializing namespace ...',function(error,result){
            if (error)
                console.log('initializing namespace ... failure');
            else if (result){
                var device = null;
                var target = null;
                var prevLine = null;
                var prevComponent = null;
                console.log('initializing namespace ... started');
                for (let entry of result.data.data){
                    console.log(entry);
                    var line = entry[0];
                    var component = entry[1];
                    if ( line != prevLine ){
                        prevLine = line;
                        prevComponent = component;
                        device = addDevice(namespace,extruder,line);
                        target = addComponent(namespace,device,component);                        
                    } else if ( component != prevComponent ) {
                        prevComponent = component;
                        target = addComponent(namespace,device,component);
                    }
                    addNextNode(namespace,target,entry);
                }
                console.log('initializing namespace ... done');
            }
        });
        
    }
    construct_my_address_space(server);
    server.start(function() {
        console.log("MIP-2-OPC-Server started.");
        console.log(" port ", server.endpoints[0].port);
        const endpointUrl = server.endpoints[0].endpointDescriptions()[0].endpointUrl;
        console.log(" the primary server endpoint url is ", endpointUrl );
    });
}
server.initialize(post_initialize);

process.on('SIGINT',function(){
    console.log("MIP-2-OPC-Server shutdown requested.");
    server.shutdown(1000, function () {
        console.log("MIP-2-OPC-Server shutdown completed.");
        process.exit(0);
    });
});

