# README #

# VIP-MIP-Controller #

## Installation ##

### Mipsock-User anlegen ###
(falls noch nicht vorhanden)

```
#!bash
sudo adduser vimicon
sudo adduser vimicon sudo
su vimicon
cd ~
```

### NVM und NodeJS installieren ###
```
#!bash
sudo apt-get update
sudo apt-get install build-essential libssl-dev git
```
-> verfuegbare releases: [https://github.com/nvm-sh/nvm/releases]
-> [https://github.com/creationix/nvm](https://github.com/creationix/nvm)
```
#!bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
nvm install 10.18
nvm use 10.18
nvm alias default 10.18
```

### ViMiCon installieren ###
```
#!bash
git clone https://githost@bitbucket.org/githost/vimicon.git
cd vimicon
git fetch && git checkout CU_[KUNDE]
make install
```

### bei Abbruch in make install (npm install)

-> Zustand der installierten Pakete pruefen: npm audit
-> ggfs. bei schweren Fehlern alles reparieren: npm audit fix --force
-> oder sukzessive dezidierte Pakete reparieren (z.B.): npm install --save-dev jest@24.8.0

### ViMiCon unit tests ausfuehren ###
```
#!bash
make test
```
./lib/config.js mit den korrekten Parametern konfigurieren.
...
cd ..

### PM2 installieren ###
```
#!bash
npm install -g pm2
pm2 startup ubuntu
sudo env PATH=$PATH:/home/vimicon/.nvm/v0.11.16/bin pm2 startup ubuntu -u vimicon
```

### Logrotate einrichten ###
```
#!bash
cd vimicon
sudo cp ./git_hooks/vimicon /etc/logrotate.d/

```

### Testen ###
```
#!bash
pm2 start pm2_vimicon.json
pm2 save
```

Reboot testen.