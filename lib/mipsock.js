/**
 * Socket Server for webMIP 2.x
 *
 * @author Marcel Rehfeld (marcel.rehfeld@inno-plast.de)
 *
 * Copyright (c) 2015 Inno-Plast GmbH
 */

var net = require('net');
var config = require('./config').socket;
var _ = require('underscore');
var async = require('async');
var server = net.createServer();
var debug = require('./debugwrap').debug('mip:sock:deb');
var er = require('./debugwrap').error('mip:sock:err');
var PORT = config.PORT;
var HOST = config.HOST;

var sockList = {};
var keywordHandleFunctions = {};

server.on('connection', function (sock) {
  var rAdd = sock.remoteAddress;
  var rPort = sock.remotePort;

  addConnectedSocket(sock);

  sock.on('data', function (data) {
    dataHandler(data, sock);
  });

  sock.on('error', errHandler);

  sock.on('close', function () {
    debug('Disconnected: ' + rAdd + ':' + rPort);
    socketCloseHandler(sock);
  });
});

server.on('error', errHandler);

/**
 * Adds a socket to the internal socketlist.
 * @param {Socket} socket the socket to add.
 */
function addConnectedSocket(socket) {
  debug('Connected: ' + socket.remoteAddress + ':' + socket.remotePort);
  sockList[socket.remoteAddress + socket.remotePort] = socket;
}

/**
 * Removes a socket from the socketlist.
 * @param  {Socket} socket the socket to remove.
 */
function removeConnectedSocket(socket) {
  delete sockList[socket.remoteAddress + socket.remotePort];
}

/**
 * Handler for close-event on socket.
 * @param  {Socket} socket the socket throwing the event.
 */
function socketCloseHandler(socket) {
  removeConnectedSocket(socket);
}

/**
 * Error wrapper: ('mip:sock:err');
 * @param  {Error} err Error Object.
 */
function errHandler(err) {
  er(err);
}

/**
 * OnData-Eventhandler for each socket.
 * @param  {Buffer} data Buffer containing the sent data.
 * @param  {Socket} sock the socket recieving the data.
 */
function dataHandler(data, sock) {
  var answerObject = {
    header: {
      datetime: Math.floor(new Date() / 1000),
      success: false
    }
  };

  data = data.toString().replace(/(\r\n|\n|\r)/gm, "");

  if (data && data !== undefined && data === 'q') {
    sock.end();
  } else {
    var parsedData = {};
    debug('Recieved:[' + sock.remoteAddress + ']: ' + data);
    try {
      parsedData = JSON.parse(data);
    } catch (e) { // json encoding error -> abort processing
      er('[' + sock.remoteAddress + '] ' + 'Json encoding failed for data: "' +
        data + '"');
      //er(e);
      return;
    }
    if (_.has(parsedData, 'header') && _.has(parsedData, 'request')) {
      if (_.has(keywordHandleFunctions, parsedData.request)) {
        var handlerParameter = null;
        if (_.has(parsedData, 'data')) {
          handlerParameter = parsedData.data;
        }
        keywordHandleFunctions[parsedData.request](handlerParameter, function (
          err, result) {
          if (err) {
            debug(err);
            answerObject.answer = err;
            answerObject.header.success = false;
          } else {
            answerObject.answer = result.data;
            answerObject.header.success = result.success;
          }
          debug('Answer to:[' + sock.remoteAddress + ']: ' + JSON.stringify(
            answerObject));
          sock.write(JSON.stringify(answerObject));
        });
      } else {
        debug('no handler defined for request');
        answerObject.answer = 'no handler defined';
        debug('Answer to:[' + sock.remoteAddress + ']: ' + JSON.stringify(
          answerObject));
        sock.write(JSON.stringify(answerObject));
      }

    } else {
      sock.write(JSON.stringify('no'));
    }
  }
}

/**
 * Starts the server.
 * @param  {Function} cb Callback on success(null) or error(err).
 */
function startServer(cb) {
  server.on('error', function (err) {
    cb(err);
  });

  server.listen(PORT, HOST, function (err) {
    debug('Server listening on ' + server.address().address + ':' +
      server.address().port);
    cb(null);
  });
}

/**
 * Closes the server. Destroys all open socket-connections.
 * @param  {Function} cb Callback on success(null) or error(err).
 */
function closeServer(cb) {

  server.close(function (err) {
    if (err) {
      cb(err);
    } else {
      cb(null);
    }
  });
  for (var key in sockList) {
    sockList[key].destroy();
  }
}

/**
 * Adds a function as a handler for a specific keyword.
 * If there already is a function registered for that keyword, an error is returned.
 * @param {String}   keyword  the keyword for the function.
 * @param {Function}   handler  the function to handle the keyword action.
 * @param {Function} callback Callback on success(null) or error(err).
 */
function addHandlerForKeyword(keyword, handler, callback) {
  if (!_.has(keywordHandleFunctions, keyword)) {
    keywordHandleFunctions[keyword] = handler;
    callback(null);
  } else {
    callback(keyword + ' is already keyword');
  }
}

/**
 * Determines if a handler-function is registered as handler for a keyword.
 * @param  {String}   keyword  the keyword for the function.
 * @param  {Function}   handler  the function to handle the keyword action.
 * @param  {Function} callback Callback on finish. (err, {Boolean: true/false})
 */
function isHandlerForKeyword(keyword, handler, callback) {
  if (_.has(keywordHandleFunctions, keyword)) {
    if (keywordHandleFunctions[keyword].name === handler.name) {
      return callback(null, true);
    } else {
      return callback(null, false);
    }
  } else {
    return callback(null, false);
  }
}

/**
 * Removes the handler-function for a specific keyword.
 * If no function for that keyword is registered, an error is returned.
 * @param  {String}   keyword  the keyword for which any handler shall be removed.
 * @param  {Function} callback Callback on success(null) or error(err).
 */
function removeHandlerForKeyword(keyword, callback) {
  if (_.has(keywordHandleFunctions, keyword)) {
    delete keywordHandleFunctions[keyword];
    return callback(null);
  } else {
    return callback(keyword + ' is not registered');
  }
}


module.exports.startServer = startServer;
module.exports.closeServer = closeServer;
module.exports.addHandlerForKeyword = addHandlerForKeyword;
module.exports.isHandlerForKeyword = isHandlerForKeyword;
module.exports.removeHandlerForKeyword = removeHandlerForKeyword;
