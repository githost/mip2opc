/**
 * Wrapper for debug module.
 *
 * @author Marcel Rehfeld (marcel.rehfeld@inno-plast.de)
 *
 * Copyright (c) 2015 Inno-Plast GmbH
 */

var deb = require('debug');
deb.formatArgs = function formatArgs() {
  var args = arguments;
  var useColors = this.useColors;
  var name = this.namespace;

  if (useColors) {
    var c = this.color;

    args[0] = '  \u001b[3' + c + ';1m' + name + ' ' + '\u001b[0m' + args[
        0] + '\u001b[3' + c + 'm' + ' +' + deb.humanize(this.diff) +
      '\u001b[0m';
  } else {
    args[0] = name + ' ' + args[0];
  }
  return args;
};

var debug = function (name) {
  var result = deb(name);
  result.log = console.log.bind(console);
  return result;
};

var error = function (name) {
  var result = deb(name);
  return result;
};

module.exports.error = error;
module.exports.debug = debug;
