/**
 * Request utility to send GET-Requests to webMIP 2.x
 *
 * @author Marcel Rehfeld (marcel.rehfeld@inno-plast.de)
 *
 * Copyright (c) 2015 Inno-Plast GmbH
 */


var request = require('request');
var config = require('./config').request;
var _ = require('underscore');
var debug = require('./debugwrap').debug('mip:sock:request:deb');
var er = require('./debugwrap').error('mip:sock:request:err');
var async = require('async');

var mip2opc = require('./opc.interface');

function opcRead(data, callback) {

  var endpointUrl = "opc.tcp://";
  
  if (data === undefined || data === null) {
    return callback('no data parameter defined');
  } else {
    endpointUrl += config.opcHost + ':';
    endpointUrl += config.opcPort + config.opcPath;
  }

  debug('requesting nodeRead: ' + endpointUrl);
  debug(data);
  mip2opc.nodeReadArray(endpointUrl,data,function(err,result){
    if (err)
    console.log('opcRead - err: '+err);
    return callback(null,result);
  });
}

function opcWrite(data, callback) {

  var endpointUrl = "opc.tcp://";
  
  if (data === undefined || data === null) {
    return callback('no data parameter defined');
  } else {
    endpointUrl += config.opcHost + ':';
    endpointUrl += config.opcPort + config.opcPath;
  }

  debug('requesting nodeWrite: ' + endpointUrl);
  debug(data);
  mip2opc.nodeWriteArray(endpointUrl,data,function(err,result){
    if (err)
    console.log('opcWrite - err: '+err);
    return callback(null,result);
  });
}

/**
 * Creates a request to targetFile in location which is defined in config.request.
 * @param  {String}   targetFile target for request
 * @param  {Object}   data       JS-Object which gets wrapped in a 'data' queryparameter.
 * @param  {Function} callback   contains (err, json-object with response-body)
 * @return {Function}              callback
 */
function doRequest(targetFile, data, callback) {

  if (data === undefined || data === null) {
    return callback('no data parameter defined');
  }

  var options = {
    url: config.urlEndpoint + targetFile,
    method: 'GET',
    qs: {
      'data': JSON.stringify(data)
    },
    timeout: 10000
  };
  debug('requesting urlEndpoint: ' + options.url);
  debug(data);
  request(options, function (err, response, body) {
    if (err) {
      return callback(err);
    }
    if (response && response.statusCode && response.statusCode === 404) {
      return callback('got statusCode 404');
    }
    try {
      var parsedBody = JSON.parse(body);
      return callback(null, parsedBody);
    } catch (e) {
      er('cannot decode request answer:' + body);
      return callback('cannot decode MIP request answer');
    }

  });
}

/**
 * Checks an answer object from webMIP 2.x /phpgetter/*.php to match the requirements.
 * @param  {Object}   reqAnswer answer-object to check
 * @param  {Function} callback  contains (err, result[boolean])
 * @return {Function}             callback
 */
function checkRequestAnswer(reqAnswer, callback) {
  if (typeof reqAnswer !== 'object') {
    return callback('answer is no object');
  }
  var propArr = ['data', 'msg', 'error'];

  async.each(propArr, function (item, cb) {
    if (!_.has(reqAnswer, item)) {
      cb('answer has no property ' + item);
    } else {
      cb();
    }
  }, function (err) {
    if (err) {
      return callback(err);
    } else {
      debug(reqAnswer.data);
      return callback(null, reqAnswer);
    }
  });


}

module.exports.opcRead = opcRead;
module.exports.opcWrite = opcWrite;
module.exports.doRequest = doRequest;
module.exports.checkRequestAnswer = checkRequestAnswer;
