var nodeArray = [];

function addNodeItem(item){
    nodeArray.push(item);
}

function getNodeItem(entry,index){
    var item = nodeArray.find(function(element){
        return (
        ( element[0] == entry.line ) &&
        ( element[1] == entry.component ) &&
        ( element[2] == entry.name[index] ) );
    });
    return item;
}

module.exports = {
    nodeArray,
    addNodeItem,
    getNodeItem
}
