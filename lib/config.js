var config = {}

config.request = {};
config.request.opcPort = 4840;
config.request.opcHost = 'localhost';
config.request.opcPath = '/mip2opc/';
config.request.urlEndpoint = process.env.URLENDPOINT ||
  'http://localhost/webmip/';

config.socket = {};
config.socket.PORT = process.env.PORT || 20003;
config.socket.HOST = process.env.HOST || 'localhost';

module.exports = config;
