var debug = require('./debugwrap').debug('mip:sock:handler:deb');
var er = require('./debugwrap').error('mip:sock:handler:err');
var _ = require('underscore');
var async = require('async');
var miprequest = require('./miprequest');

    function select(params, cb) {
      var targetFile = 'phpgetter/get_json_mip2opc.php';

      async.waterfall([
        function (callback) {
          miprequest.doRequest(targetFile, params, callback);
        },
        function (result, callback) {
          miprequest.checkRequestAnswer(result, callback);
        }
      ], function (err, result) {
        if (err) {
          return cb(err);
        } else {
          // debug(result);
          if (result.error.length > 0) {
            er(result.error);
          }
          return cb(null, {
            data: result.data,
            success: result.data.success
          });
        }
      });
    }

module.exports.select = select;
