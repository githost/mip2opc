/**
 * Socket Server Handler Functions for webMIP 2.x
 *
 * @author Marcel Rehfeld (marcel.rehfeld@inno-plast.de)
 *
 * Copyright (c) 2015 Inno-Plast GmbH
 */

var debug = require('./debugwrap').debug('mip:sock:handler:deb');
var er = require('./debugwrap').error('mip:sock:handler:err');
var _ = require('underscore');
var async = require('async');
var miprequest = require('./miprequest');

const MIP2OPC_MODE_READ  = 0;
const MIP2OPC_MODE_WRITE = 1;

/**
 * Requests the opcua server with the given parameter.
 * Read node variable information.
 * @param  {Object}   params   request parameter.
 * @param  {Function} cb Callback on success(null, result) or error(err).
 * @return {Callback}            [description]
 */
function opcRead(params, cb) {

  async.waterfall([
    function (callback) {
      checkParams(params, callback, MIP2OPC_MODE_READ);
    },
    function (callback) {
      miprequest.opcRead(params, callback);
    },
    function (result, callback) {
      miprequest.checkRequestAnswer(result, callback);
    }
  ], function (err, result) {
    if (err) {
      return cb(err);
    } else {
      // debug(result);
      if (result.error.length > 0) {
        er(result.error);
      }
      return cb(null, {
        data: result.data,
        success: result.data.success
      });
    }
  });
}

/**
 * Requests the opcua server with the given parameter.
 * Update node variable information.
 * @param  {Object}   params   request parameter.
 * @param  {Function} cb Callback on success(null, result) or error(err).
 * @return {Callback}            [description]
 */
function opcWrite(params, cb) {

  async.waterfall([
    function (callback) {
      checkParams(params, callback, MIP2OPC_MODE_WRITE);
    },
    function (callback) {
      miprequest.opcWrite(params, callback);
    },
    function (result, callback) {
      miprequest.checkRequestAnswer(result, callback);
    }
  ], function (err, result) {
    if (err) {
      return cb(err);
    } else {
      // debug(result);
      if (result.error.length > 0) {
        er(result.error);
      }
      return cb(null, {
        data: result.data,
        success: result.data.success
      });
    }
  });
}

/**
 * Helper function for checking opc_read/opc_write parameter.
 * If base, line, component, name or value (opc_write) are 
 * not in params, an error is thrown.
 * @param  {Object}   params   parameter object to test.
 * @param  {Function} callback err on error.
 * @return {Function}            callback
 */
function checkParams(params, callback, mode) {
  switch (mode){
    case MIP2OPC_MODE_WRITE :
      if (!_.has(params, 'value')) {
        return callback('value not in parameters');
      }
    case MIP2OPC_MODE_READ  :
    default :
      if (!_.has(params, 'base')) {
        return callback('base not in parameters');
      }
      if (!_.has(params, 'line')) {
        return callback('line not in parameters');
      }
      if (!_.has(params, 'component')) {
        return callback('component not in parameters');
      }
      if (!_.has(params, 'name')) {
        return callback('name not in parameters');
      }
  }
  return callback(null);
}

module.exports.opcRead = opcRead;
module.exports.opcWrite = opcWrite;

