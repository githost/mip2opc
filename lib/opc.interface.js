const async = require("async");
const opcua = require("node-opcua");

var globals = require("./globals.js");

    function nodeRead(url,entry,cb) {

        var client = opcua.OPCUAClient.create({
            endpoint_must_exist: false
        });
        client.on("backoff", (retry, delay) =>
          console.log(
            "still trying to connect to ",
            url,
            ": retry =",
            retry,
            "next attempt in ",
            delay / 1000,
            "seconds"
          )
        );

        var result = [];
        let the_session;

        async.series([

            // step 1 : connect to
            function(callback)  {
                client.connect(url, function(err) {
                  if (err) {
                    console.log(" cannot connect to endpoint :", url);
                  } else {
                    console.log("connected !");
                  }
                  callback(err);
                });
            },

            // step 2 : createSession
            function(callback) {
                client.createSession(function(err, session) {
                  if (err) {
                    return callback(err);
                  }
                  the_session = session;
                  callback();
                });
            },

            // step 3' : read a variable with read
            function(callback) {

               var nodeId  = entry.base.join(';');
               nodeId += '.' + entry.line;
               nodeId += '.' + entry.component;
               nodeId += '.' + entry.name;

                 the_session.readVariableValue(nodeId, function(err, dataValue) {
                   if (!err) {
                     console.log("Reading nodeId: "+nodeId);
                     result.push( { 
                         value:  dataValue.value.value,
                         type:   dataValue.value.dataType,
                         name:   dataValue.statusCode.name,
                         status: dataValue.statusCode.value
                     } );
                   }
                   else callback(err);
                 });
               callback();
            },

            // close session
            function(callback) {
                the_session.close(function(err) {
                  if (err) {
                    console.log("closing session failed ?");
                  }
                  callback();
                });
            }

        ],
        function(err) {
            if (err) {
                console.log(" failure ",err);
            } else {
                console.log("done!");
                cb(err,{ data: result, error: [], msg: ['nodeRead: done'] });
            }
            client.disconnect(function(){});
        }) ;
        
    }

    function nodeReadArray(url,entry,cb) {

        var client = opcua.OPCUAClient.create({
            endpoint_must_exist: false
        });
        client.on("backoff", (retry, delay) =>
          console.log(
            "still trying to connect to ",
            url,
            ": retry =",
            retry,
            "next attempt in ",
            delay / 1000,
            "seconds"
          )
        );

        var result = [];
        let the_session;

        async.series([

            // step 1 : connect to
            function(callback)  {
                client.connect(url, function(err) {
                  if (err) {
                    console.log(" cannot connect to endpoint :", url);
                  } else {
                    console.log("connected !");
                  }
                  callback(err);
                });
            },

            // step 2 : createSession
            function(callback) {
                client.createSession(function(err, session) {
                  if (err) {
                    return callback(err);
                  }
                  the_session = session;
                  callback();
                });
            },

            // step 3' : read a variable with read
            function(callback) {

               var nodeName;
               var nodesToRead = [];
               entry.name.forEach(function(item,index){
                   
                   nodeName  = entry.base.join(';');
                   nodeName += '.' + entry.line;
                   nodeName += '.' + entry.component;
                   nodeName += '.' + entry.name[index];
                   
                   nodesToRead.push({
                       nodeId: nodeName,
                       attributeId: opcua.AttributeIds.Value
                   });
               });

                 the_session.read(nodesToRead, function(err, dataValue) {
                   if (!err) {
                     console.log("nodeReadArray - line/component: "+entry.line+'/'+entry.component);
                     dataValue.forEach(function(item){
                         result.push({
                             value: item.value.value,
                             type:  item.value.dataType,
                             name:  item.statusCode.name,
                             status: item.statusCode.value
                         });
                     });
                   }
                   else callback(err);
                 });
               callback();
            },

            // close session
            function(callback) {
                the_session.close(function(err) {
                  if (err) {
                    console.log("closing session failed ?");
                  }
                  callback();
                });
            }

        ],
        function(err) {
            if (err) {
                console.log(" failure ",err);
            } else {
                console.log("done!");
                cb(err,{ data: result, error: [], msg: ['nodeRead: done'] });
            }
            client.disconnect(function(){});
        }) ;
        
    }

    function nodeWrite(url,entry,cb) {

        var client = opcua.OPCUAClient.create({
            endpoint_must_exist: false
        });
        client.on("backoff", (retry, delay) =>
          console.log(
            "still trying to connect to ",
            url,
            ": retry =",
            retry,
            "next attempt in ",
            delay / 1000,
            "seconds"
          )
        );

        var result = [];
        let the_session;

        async.series([

            // step 1 : connect to
            function(callback)  {
                client.connect(url, function(err) {
                  if (err) {
                    console.log(" cannot connect to endpoint :", url);
                  } else {
                    console.log("connected !");
                  }
                  callback(err);
                });
            },

            // step 2 : createSession
            function(callback) {
                client.createSession(function(err, session) {
                  if (err) {
                    return callback(err);
                  }
                  the_session = session;
                  callback();
                });
            },

            // step 3' : update a variable with write
            function(callback) {

               var dataToWrite = { 
                   dataType: 'Double', 
                   value: entry.value };
               var nodeId = entry.base.join(';');
               
               nodeId += '.' + entry.line;
               nodeId += '.' + entry.component;
               nodeId += '.' + entry.name;

               the_session.writeSingleNode(nodeId,dataToWrite,function(err,code){
                   if (!err) {
                     console.log("Writing nodeId: "+nodeId);
                     result.push( { 
                         name:  code.name,
                         status: code.value
                     } );
                   }
                   else callback(err);
                 });
               callback();
            },

            // close session
            function(callback) {
                the_session.close(function(err) {
                  if (err) {
                    console.log("closing session failed ?");
                  }
                  callback();
                });
            }

        ],
        function(err) {
            if (err) {
                console.log(" failure ",err);
            } else {
                console.log("done!");
                cb(err,{ data: result, error: [], msg: ['nodeWrite: done'] });
            }
            client.disconnect(function(){});
        }) ;
        
    }
    
    function nodeWriteArray(url,entry,cb) {

        var client = opcua.OPCUAClient.create({
            endpoint_must_exist: false
        });
        client.on("backoff", (retry, delay) =>
          console.log(
            "still trying to connect to ",
            url,
            ": retry =",
            retry,
            "next attempt in ",
            delay / 1000,
            "seconds"
          )
        );

        var result = [];
        let the_session;

        async.series([

            // step 1 : connect to
            function(callback)  {
                client.connect(url, function(err) {
                  if (err) {
                    console.log(" cannot connect to endpoint :", url);
                  } else {
                    console.log("connected !");
                  }
                  callback(err);
                });
            },

            // step 2 : createSession
            function(callback) {
                client.createSession(function(err, session) {
                  if (err) {
                    return callback(err);
                  }
                  the_session = session;
                  callback();
                });
            },

            // step 3' : update a variable with write
            function(callback) {
                
               var nodesToWrite = [];
               var dataItem, nodeName;
               entry.value.forEach(function(item,index){
                   
                   var element = globals.getNodeItem(entry,index);
                   if (element) {
                       
                       var dataType = 
                           element.length > 4 ? 
                           element[4].toLowerCase() : 'default';

                       switch (dataType){
                           case 'integer' : dataType = 'Int32';  break;
                           case 'short'   : dataType = 'Int16';  break;
                           case 'float'   : dataType = 'Float';  break;
                           case 'string'  : dataType = 'String'; break;
                           default        : dataType = 'Double'; break;
                       }
                       
                       dataItem = {
                           value: item,
                           dataType: dataType };

                       nodeName  = entry.base.join(';');
                       nodeName += '.' + entry.line;
                       nodeName += '.' + entry.component;
                       nodeName += '.' + entry.name[index];

                       nodesToWrite.push({
                           nodeId: nodeName,
                           attributeId: opcua.AttributeIds.Value,
                           value: { value: dataItem }
                       });
                   }
               });

               the_session.write(nodesToWrite,function(err,code){
                   if (!err) {
                     var nameArray = [];
                     var statusArray = [];
                     code.forEach(function(item){
                         nameArray.push(item.name);
                         statusArray.push(item.value);
                     })
                     console.log("nodeWriteArray - line/component: "+entry.line+'/'+entry.component);
                     result.push( { 
                         name:  nameArray,
                         status: statusArray
                     } );
                   }
                   else callback(err);
                 });
               callback();
            },

            // close session
            function(callback) {
                the_session.close(function(err) {
                  if (err) {
                    console.log("closing session failed ?");
                  }
                  callback();
                });
            }

        ],
        function(err) {
            if (err) {
                console.log(" failure ",err);
            } else {
                console.log("done!");
                cb(err,{ data: result, error: [], msg: ['nodeWrite: done'] });
            }
            client.disconnect(function(){});
        }) ;
        
    }
    
//module.exports.nodeRead = nodeRead;
//module.exports.nodeWrite = nodeWrite;
module.exports.nodeReadArray = nodeReadArray;
module.exports.nodeWriteArray = nodeWriteArray;
