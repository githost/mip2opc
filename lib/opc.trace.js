    function trace(server) {

        server.on("create_session", function (session) {
            var name = session.clientDescription.applicationName;
            console.log("MIP-2-OPC-Server session created: "+name.text);
        });

        server.on("session_closed", function (session, reason) {
            var name = session.clientDescription.applicationName;
            console.log("MIP-2-OPC-Server session closed: "+name.text);
        });

        server.on("newChannel",function(channel) {
            var port = channel.remotePort;
            var address = channel.remoteAddress;
            console.log("MIP-2-OPC-Server connected to: ",address," // ",port);
        });

        server.on("closeChannel",function(channel) {
            var port = channel.remotePort;
            var address = channel.remoteAddress;
            console.log("MIP-2-OPC-Server disconnected: ",address," // ",port);
        });

        server.on("request", function (request, channel) {
            if (request)
            console.log("MIP-2-OPC-Server request: ",request.toString());
            if (channel)
            console.log("MIP-2-OPC-Server channel: ",channel.toString());
        });

        server.on("response", function (response) {
            if (response)
            console.log("MIP-2-OPC-Server response: ",response.toString());
        });
    }

module.exports.trace = trace;
