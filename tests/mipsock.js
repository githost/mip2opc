/**
 * Tests for mipsock.js
 *
 * @author Marcel Rehfeld (marcel.rehfeld@inno-plast.de)
 *
 * Copyright (c) 2015 Inno-Plast GmbH
 */
var libpath = process.env.LIB_COV ? '../lib-cov' : '../lib';
var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var proxyquire = require('proxyquire');
var net = require('net');
var debug = require('debug')('mip:test:sock:deb');
var er = require('debug')('mip:test:sock:err');
var config = require('../lib/config').socket;
var mipsock = require(libpath + '/mipsock');

chai.config.includeStack = true;

describe('mipsock', function () {
  var PORT = config.PORT;
  var HOST = config.HOST;
  var client;
  before(function () {

  });

  describe('#startServer', function () {


    beforeEach(function () {


    });

    it('should allow the server to start on host and port',
      function (done) {

        mipsock.startServer(function (err) {
          /*jshint -W030 */
          expect(err).to.be.null;
          mipsock.closeServer(function (err) {
            /*jshint -W030 */
            expect(err).to.be.null;
            done();
          });
        });
      }
    );

    it('should allow a client to connect on host and port',
      function (done) {
        /*jshint -W030 */
        client = new net.Socket();
        mipsock.startServer(function (err) {
          expect(err).to.be.null;

          client.connect(PORT, HOST, function () {
            debug('client connected');
          });

          client.on('connect', function () {
            debug('destroy client');
            client.destroy();
          });

          client.on('close', function () {
            debug('Client closed');
            mipsock.closeServer(function (err) {
              expect(err).to.be.null;
              done();
            });
          });

          client.on('error', function (err) {
            /*jshint -W030 */
            expect(err).to.be.null;

            done();
          });
        });
      }
    );
  });



  describe('#closeServer', function () {


    it('should close the server',
      function (done) {
        /*jshint -W030 */
        mipsock.startServer(function (err) {
          expect(err).to.be.null;
          mipsock.closeServer(function (err) {

            expect(err).to.be.null;
            done();
          });
        });
      }
    );

    it('should close the client\'s connection',
      function (done) {
        /*jshint -W030 */
        client = new net.Socket();
        mipsock.startServer(function (err) {
          expect(err).to.be.null;

          client.connect(PORT, HOST, function () {
            debug('client connected');
          });

          client.on('connect', function () {
            mipsock.closeServer(function (err) {
              expect(err).to.be.null;
            });
          });

          client.on('close', function () {
            debug('Client closed');
            done();
          });

          client.on('error', function (err) {
            /*jshint -W030 */
            expect(err).to.be.null;
            done();
          });
        });
      }
    );
  });

  describe('#addHandlerForKeyword', function () {


    it('should add a handler for a Keyword',
      function (done) {
        /*jshint -W030 */
        function aliveHandler(data) {
          return "alive";
        }
        mipsock.addHandlerForKeyword('alive', aliveHandler, function (
          err) {
          expect(err).to.be.null;
          done();
        });
      });

    it('should error if a handler for a keyword is already registered',
      function (done) {
        /*jshint -W030 */
        function aliveHandler(data) {
          return "alive";
        }
        mipsock.addHandlerForKeyword('alive', aliveHandler, function (
          err) {
          expect(err).not.to.be.null;
          expect(err).to.equal('alive is already keyword');
          done();
        });
      });

    after(function (done) {
      /*jshint -W030 */
      mipsock.removeHandlerForKeyword('alive', function (err) {
        expect(err).to.be.null;
        done();
      });
    });
  });

  describe('#isHandlerForKeyword', function () {
    var aH = function aliveHandler(data) {
      return "alive";
    };

    var aH2 = function aliveHandler2(data) {
      return "alive";
    };
    before(function (done) {
      mipsock.addHandlerForKeyword('alive', aH, function (
        err) {
        /*jshint -W030 */
        expect(err).to.be.null;
        done();
      });
    });

    it(
      'should return a true and no error if a specific handler for a Keyword is registered',
      function (done) {
        /*jshint -W030 */
        mipsock.isHandlerForKeyword('alive', aH, function (
          err, isHandler) {
          expect(err).to.be.null;
          expect(isHandler).to.be.true;
          done();
        });
      });

    it(
      'should return false if a Keyword is not found',
      function (done) {
        /*jshint -W030 */
        mipsock.isHandlerForKeyword('otherFail', aH,
          function (
            err, isHandler) {
            expect(err).to.be.null;
            expect(isHandler).to.be.false;
            done();
          });
      });

    it(
      'should return false if a handler for a Keyword is NOT registered',
      function (done) {
        /*jshint -W030 */
        mipsock.isHandlerForKeyword('alive',
          aH2,
          function (
            err, isHandler) {
            expect(err).to.be.null;
            expect(isHandler).to.be.false;
            done();
          });
      });
    after(function (done) {
      /*jshint -W030 */
      mipsock.removeHandlerForKeyword('alive', function (err) {
        expect(err).to.be.null;
        done();
      });
    });
  });

  describe('#removeHandlerForKeyword', function () {
    var handlerToRemove = function handlerToRemove(data) {
      return "remove me";
    };

    beforeEach(function (done) {
      /*jshint -W030 */
      mipsock.addHandlerForKeyword('toRemove', handlerToRemove,
        function (err) {
          expect(err).to.be.null;
          mipsock.isHandlerForKeyword('toRemove', handlerToRemove,
            function (err, isHandler) {
              expect(err).to.be.null;
              expect(isHandler).to.be.true;
              done();
            });

        });
    });

    it(
      'should remove a handler for a Keyword',
      function (done) {
        /*jshint -W030 */
        mipsock.removeHandlerForKeyword('toRemove', function (
          err) {
          expect(err).to.be.null;
          mipsock.isHandlerForKeyword('toRemove', handlerToRemove,
            function (err, isHandler) {
              expect(err).to.be.null;
              expect(isHandler).to.be.false;
              done();
            });
        });
      });

    it(
      'should return true if a handler for a Keyword could be removed',
      function (done) {
        /*jshint -W030 */
        mipsock.removeHandlerForKeyword('toRemove', function (
          err) {
          expect(err).to.be.null;
          done();
        });
      });

    it(
      'should return false if a handler for a Keyword could NOT be removed',
      function (done) {
        /*jshint -W030 */
        mipsock.removeHandlerForKeyword('notThere', function (
          err) {
          expect(err).to.equal('notThere is not registered');
          done();
        });
      });

    //cleanup
    after(function (done) {
      /*jshint -W030 */
      mipsock.removeHandlerForKeyword('toRemove',
        function (err) {
          if (err) {
            expect(err).to.equal('toRemove is not registered');
          }
          done();
        });
    });
  });
  describe('#requesting the server', function () {

    before(function (done) {
      /*jshint -W030 */

      mipsock.startServer(function (err) {
        expect(err).to.be.null;
        done();
        client.on('error', function (err) {
          /*jshint -W030 */
          expect(err).to.be.null;
        });
      });

    });

    beforeEach(function (done) {
      client = new net.Socket();
      client.connect(PORT, HOST, function () {
        done();
      });
    });

    it(
      'should timeout if request is in wrong format (no json) and not parseable',
      function (done) {
        var EXPECTED_TIMEOUT = 200;
        this.timeout(EXPECTED_TIMEOUT + 100);
        var timeout = setTimeout(done, EXPECTED_TIMEOUT);

        client.on('data', function (data) {
          /*jshint -W030 */
          clearTimeout(timeout);
          done(new Error('This must not happen'));
        });

        client.write('some wrong format {{{');
      });

    it(
      'should return "no" if request is parseable but in wrong format (no header+request attribute)',
      function (done) {

        client.on('data', function (data) {
          /*jshint -W030 */
          expect(data.toString()).to.equal(JSON.stringify("no"));
          done();
        });
        var dummyObj = {
          something: "parseable"
        };
        client.write(JSON.stringify(dummyObj));
      });

    it(
      'should return the specified answer object',
      function (done) {
        var requestObject = {
          header: {
            datetime: 123,
            nodeid: 12,
            serialnr: 3312
          },
          request: "dothis"
        };

        client.on('data', function (data) {
          /*jshint -W030 */
          var datResult = JSON.parse(data.toString());
          expect(datResult)
            .to.have.all.keys(['header', 'answer']);
          expect(datResult.header)
            .to.have.all.keys(['datetime', 'success']);
          assert.isString(datResult.answer);
          assert.isNumber(datResult.header.datetime);
          assert.isBoolean(datResult.header.success);
          expect(datResult.answer).to.equal('no handler defined');
          done();
        });

        client.write(JSON.stringify(requestObject));
      });

    afterEach(function (done) {
      client.destroy();
      done();
    });
    //cleanup
    after(function (done) {
      /*jshint -W030 */
      mipsock.closeServer(function (err) {
        expect(err).to.be.null;
        done();
      });
    });
  });

  describe('#Sending "q" to the server', function () {

    before(function (done) {
      /*jshint -W030 */
      mipsock.startServer(function (err) {
        expect(err).to.be.null;
        client.on('error', function (err) {
          expect(err).to.be.null;
        });
        client = new net.Socket();
        client.connect(PORT, HOST, function () {
          done();
        });
      });

    });


    it(
      'should close the socket connection.',
      function (done) {
        client.on('close', function (data) {
          /*jshint -W030 */
          done();
        });

        client.write('q');
      });

    //cleanup
    after(function (done) {
      /*jshint -W030 */
      mipsock.closeServer(function (err) {
        expect(err).to.be.null;
        done();
      });
    });

  });

  describe('#Starting the server', function () {
    var configFail = {};
    configFail.socket = {};
    configFail.socket.PORT = '80';
    configFail.socket.HOST = '::';

    var mipsockFail;

    before(function (done) {
      /*jshint -W030 */
      mipsockFail = proxyquire(libpath + '/mipsock', {
        './config': configFail
      });
      done();
    });

    it(
      'should error if configuration is wrong',
      function (done) {
        /*jshint -W030 */
        mipsockFail.startServer(function (err) {
          expect(err).not.to.be.null;
          debug(err);
          done();
        });
      });

    //cleanup
    after(function (done) {
      /*jshint -W030 */
      done();
    });
  });

  describe('#Closing the server', function () {

    before(function (done) {
      /*jshint -W030 */

      done();
    });

    it(
      'should error if server is not running',
      function (done) {
        /*jshint -W030 */
        mipsock.closeServer(function (err) {
          expect(err).not.to.be.null;
          debug(err);
          done();
        });
      });

    //cleanup
    after(function (done) {
      /*jshint -W030 */
      done();
    });
  });

});