/**
 * Tests for miprequest.js
 *
 * @author Marcel Rehfeld (marcel.rehfeld@inno-plast.de)
 *
 * Copyright (c) 2015 Inno-Plast GmbH
 */
var libpath = process.env.LIB_COV ? '../lib-cov' : '../lib';
var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
var proxyquire = require('proxyquire');
var config = require('../lib/config').request;
var debug = require('debug')('mip:test:sockreq:deb');
var er = require('debug')('mip:test:sockreq:err');
var qs = require('querystring');

//chai.config.includeStack = true;

describe('miprequest', function () {
  var miprequest;
  var request;
  before(function () {
    request = sinon.stub();
    miprequest = proxyquire(libpath + '/miprequest', {
      'request': request
    });
  });

  describe('#doRequest', function () {
    var phpTarget;
    var dataPar;
    var options;
    var body;

    beforeEach(function () {
      phpTarget = 'test.php';
      dataPar = {
        'getsomething': true,
        'dadada': false
      };

      options = {
        url: config.urlEndpoint + phpTarget,
        method: 'GET',
        qs: {
          'data': JSON.stringify(dataPar)
        },
        timeout: 10000
      };

      body = JSON.stringify({
        count: 9,
        url: "http://some-url.com/"
      });


    });

    it('should request the given target in the default location',
      function (done) {
        request.withArgs(options).yields(null, null,
          body);

        miprequest.doRequest(phpTarget, dataPar, function (err,
          data) {
          /*jshint -W030 */
          expect(err).to.be.null;
          expect(JSON.stringify(data)).to.equal(body);
          done();
        });
      }
    );
    it('should return an error if no data-Parameter set',
      function (done) {

        miprequest.doRequest(phpTarget, null, function (err,
          data) {
          /*jshint -W030 */
          expect(err).not.to.be.null;
          expect(data).to.be.undefined;
          done();
        });
      }
    );

    it('should return an error if body is not parseable',
      function (done) {
        options.url = config.urlEndpoint + 'notParseable.php';

        request.withArgs(options).yields(null, null,
          'not json parseable body}{');

        miprequest.doRequest('notParseable.php', dataPar, function (
          err,
          data) {
          /*jshint -W030 */
          expect(err).not.to.be.null;
          expect(data).to.be.undefined;
          done();
        });
      }
    );

    it('should return an error if there is no such target-file',
      function (done) {
        options.url = config.urlEndpoint + 'someuseless.php';
        var response = {};
        response.statusCode = 404;
        request.withArgs(options).yields(null, response, '{}');

        miprequest.doRequest('someuseless.php', dataPar, function (
          err,
          data) {
          /*jshint -W030 */
          expect(err).not.to.be.null;
          expect(err).to.equal('got statusCode 404');
          expect(data).to.be.undefined;
          done();
        });
      }
    );
    it('should return an error if request errors',
      function (done) {
        options.url = config.urlEndpoint + 'makeErr';
        request.withArgs(options).yields('no options', null,
          null);

        miprequest.doRequest('makeErr', dataPar, function (err,
          data) {
          /*jshint -W030 */
          expect(err).not.to.be.null;
          expect(err).to.equal('no options');
          expect(data).to.be.undefined;
          done();
        });
      }
    );
  });

  describe('#checkRequestAnswer', function () {

    beforeEach(function () {

    });

    it(
      'should return an error if answer object is no object',
      function (done) {
        var testObject = '';
        miprequest.checkRequestAnswer(testObject, function (err) {
          /*jshint -W030 */
          expect(err).not.to.be.null;
          expect(err).to.equal('answer is no object');
          done();
        });

      });
    it(
      'should return an error if answer object has no data-property',
      function (done) {
        var testObject = {
          msg: [],
          error: []
        };
        miprequest.checkRequestAnswer(testObject, function (err) {
          /*jshint -W030 */
          expect(err).not.to.be.null;
          expect(err).to.equal('answer has no property data');
          done();
        });

      });
    it(
      'should return an error if answer object has no msg-property',
      function (done) {
        var testObject = {
          data: [],
          error: []
        };
        miprequest.checkRequestAnswer(testObject, function (err) {
          /*jshint -W030 */
          expect(err).not.to.be.null;
          expect(err).to.equal('answer has no property msg');
          done();
        });

      });
    it(
      'should return an error if answer object has no error-property',
      function (done) {
        var testObject = {
          data: [],
          msg: []
        };
        miprequest.checkRequestAnswer(testObject, function (err) {
          /*jshint -W030 */
          expect(err).not.to.be.null;
          expect(err).to.equal('answer has no property error');
          done();
        });

      });

    it(
      'should return the object if object matches requirements',
      function (done) {
        var testObject = {
          data: [],
          msg: [],
          error: []
        };
        miprequest.checkRequestAnswer(testObject, function (err,
          result) {
          /*jshint -W030 */
          expect(err).to.be.null;
          expect(result).to.deep.equal(testObject);
          done();
        });

      });
  });
});