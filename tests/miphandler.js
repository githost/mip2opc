/**
 * Tests for miphandler.js
 *
 * @author Marcel Rehfeld (marcel.rehfeld@inno-plast.de)
 *
 * Copyright (c) 2015 Inno-Plast GmbH
 */
var libpath = process.env.LIB_COV ? '../lib-cov' : '../lib';
var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
var debug = require('debug')('mip:test:sock:handler:deb');
var er = require('debug')('mip:test:sock:handler:err');
var proxyquire = require('proxyquire');
var sinon = require('sinon');
var miphandler = require(libpath + '/miphandler');

chai.config.includeStack = true;

describe('miphandler', function () {

  describe('#newmat', function () {
    var miphandlerprox;
    var miprequeststub;
    before(function () {
      miprequeststub = {
        doRequest: sinon.stub()
      };
      miphandlerprox = proxyquire(libpath + '/miphandler', {
        './miprequest': miprequeststub
      });
    });


    it('should request the webmip server and return the success',
      function (done) {
        var matParameter = {
          rawmatid: 12322,
          silolnr: 222
        };
        miprequeststub.doRequest.withArgs('phpsetter/set_mipsilorawmat.php',
          matParameter).yields(
          null, {
            data: 1,
            msg: ['simplemsg'],
            error: []
          });
        miphandlerprox.newmat(matParameter, function (err,
          result) {
          /*jshint -W030 */
          expect(err).to.be.null;
          expect(result)
            .to.have.all.keys(['data', 'success']);
          debug(result);
          done();
        });
      }
    );

    it('should error on wrong rawmatid type',
      function (done) {
        var matParameter = {
          rawmatid: '123',
          silolnr: 22
        };
        miprequeststub.doRequest.withArgs('phpsetter/set_mipsilorawmat.php',
          matParameter).yields(
          null, {
            data: 1,
            msg: ['simplemsg'],
            error: []
          });
        miphandlerprox.newmat(matParameter, function (err,
          result) {
          /*jshint -W030 */
          debug(err);
          expect(err).to.equal('rawmatid is not a number');
          done();
        });
      }
    );
    it('should error on wrong silolnr type',
      function (done) {
        var matParameter = {
          rawmatid: 123,
          silolnr: '22'
        };
        miprequeststub.doRequest.withArgs('phpsetter/set_mipsilorawmat.php',
          matParameter).yields(
          null, {
            data: 1,
            msg: ['simplemsg'],
            error: []
          });
        miphandlerprox.newmat(matParameter, function (err,
          result) {
          /*jshint -W030 */
          debug(err);
          expect(err).to.equal('silolnr is not a number');
          done();
        });
      }
    );
    it('should error on missing parameter silolnr',
      function (done) {
        var matParameter = {
          rawmatid: 123
        };
        miprequeststub.doRequest.withArgs('phpsetter/set_mipsilorawmat.php',
          matParameter).yields(
          null, {
            data: 1,
            msg: ['simplemsg'],
            error: []
          });
        miphandlerprox.newmat(matParameter, function (err,
          result) {
          /*jshint -W030 */
          debug(err);
          expect(err).to.equal('silolnr not in parameters');
          done();
        });
      }
    );
    it('should error on missing parameter rawmatid',
      function (done) {
        var matParameter = {
          silolnr: 22
        };
        miprequeststub.doRequest.withArgs('phpsetter/set_mipsilorawmat.php',
          matParameter).yields(
          null, {
            data: 1,
            msg: ['simplemsg'],
            error: []
          });
        miphandlerprox.newmat(matParameter, function (err,
          result) {
          /*jshint -W030 */
          debug(err);
          expect(err).to.equal('rawmatid not in parameters');
          done();
        });
      }
    );
    it('should error if internal request errors',
      function (done) {
        var matParameter = {
          silolnr: 22,
          rawmatid: 123
        };
        miprequeststub.doRequest.withArgs('phpsetter/set_mipsilorawmat.php',
          matParameter).yields(
          'internal error');
        miphandlerprox.newmat(matParameter, function (err,
          result) {
          /*jshint -W030 */
          debug(err);
          expect(err).to.equal('internal error');
          done();
        });
      }
    );
    it('should error if internal requestcheck errors',
      function (done) {
        var matParameter = {
          silolnr: 22,
          rawmatid: 123
        };
        miprequeststub.doRequest.withArgs('phpsetter/set_mipsilorawmat.php',
          matParameter).yields(
          null, {
            // missing for checkfail data: 1,
            msg: ['simplemsg'],
            error: []
          });
        miphandlerprox.newmat(matParameter, function (err,
          result) {
          /*jshint -W030 */
          debug(err);
          expect(err).not.to.be.null;
          done();
        });
      }
    );
    it('should not error if internal request returns resulterror',
      function (done) {
        var matParameter = {
          silolnr: 22,
          rawmatid: 123
        };
        miprequeststub.doRequest.withArgs('phpsetter/set_mipsilorawmat.php',
          matParameter).yields(
          null, {
            data: 1,
            msg: ['simplemsg'],
            error: ['some error for test']
          });
        miphandlerprox.newmat(matParameter, function (err,
          result) {
          /*jshint -W030 */
          debug(err);
          expect(err).to.be.null;
          debug(result);
          done();
        });
      }
    );
  });
});