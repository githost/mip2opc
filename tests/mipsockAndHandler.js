/**
 * Tests for mipsock.js
 *
 * @author Marcel Rehfeld (marcel.rehfeld@inno-plast.de)
 *
 * Copyright (c) 2015 Inno-Plast GmbH
 */
var libpath = process.env.LIB_COV ? '../lib-cov' : '../lib';
var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var proxyquire = require('proxyquire');
var net = require('net');
var debug = require('debug')('mip:test:sock:deb');
var er = require('debug')('mip:test:sock:err');
var config = require('../lib/config').socket;


chai.config.includeStack = true;

describe('mipsockAndHandler', function () {
  var PORT = config.PORT;
  var HOST = config.HOST;
  var client;

  describe('#adding any handler', function () {
    var mipsock = require(libpath + '/mipsock');
    var handlerToUse = function handlerToUse(data, callback) {
      var res = {};
      res.data = {};
      res.data.success = 0;
      return callback(null, res);
    };

    var requestObject = {
      header: {
        datetime: 123,
        nodeid: 12,
        serialnr: 3312
      },
      request: "dothis"
    };

    before(function (done) {
      /*jshint -W030 */
      mipsock.addHandlerForKeyword('dothis', handlerToUse,
        function (err) {
          expect(err).to.be.null;
          mipsock.isHandlerForKeyword('dothis', handlerToUse,
            function (err, isHandler) {
              expect(err).to.be.null;
              expect(isHandler).to.be.true;
              client = new net.Socket();
              mipsock.startServer(function (err) {
                expect(err).to.be.null;
                done();
                client.on('error', function (err) {
                  expect(err).to.be.null;
                });
              });
            });
        });
    });

    it(
      'should use the added handler',
      function (done) {
        /*jshint -W030 */
        client.connect(PORT, HOST, function () {

        });
        client.on('data', function (data) {

          debug('DATA: ' + data);
          // Close the client socket completely
          client.destroy();
          done();
        });

        client.on('connect', function () {
          client.write(JSON.stringify(requestObject));
        });
      });

    //cleanup
    after(function (done) {
      /*jshint -W030 */
      mipsock.closeServer(function (err) {
        expect(err).to.be.null;
        done();
      });
    });
  });

  describe('#adding the alive handler', function () {
    var mipsock = require(libpath + '/mipsock');
    var miphandler = require(libpath + '/miphandler')
    var handlerToUse = miphandler.alive;

    var requestObject = {
      header: {
        datetime: 123,
        nodeid: 12,
        serialnr: 3312
      },
      request: "alive"
    };

    before(function (done) {
      /*jshint -W030 */
      mipsock.addHandlerForKeyword('alive', handlerToUse,
        function (err) {
          expect(err).to.be.null;
          mipsock.isHandlerForKeyword('alive', handlerToUse,
            function (err, isHandler) {
              expect(err).to.be.null;
              expect(isHandler).to.be.true;
              client = new net.Socket();
              mipsock.startServer(function (err) {
                expect(err).to.be.null;
                done();
                client.on('error', function (err) {
                  expect(err).to.be.null;
                });
              });
            });
        });
    });

    it(
      'should use the alive handler',
      function (done) {
        /*jshint -W030 */
        client.connect(PORT, HOST, function () {

        });
        client.on('data', function (data) {

          var datResult = JSON.parse(data.toString());
          debug('DATA: ' + data);
          var expectedTimestamp = Math.floor(new Date() / 1000);
          expect(parseInt(datResult.answer)).to.be.within(
            expectedTimestamp - 1,
            expectedTimestamp + 1);
          expect(datResult.header.success).to.be.true;
          // Close the client socket completely
          client.destroy();
          done();
        });

        client.on('connect', function () {
          client.write(JSON.stringify(requestObject));
        });
      });

    //cleanup
    after(function (done) {
      /*jshint -W030 */
      mipsock.closeServer(function (err) {
        expect(err).to.be.null;
        done();
      });
    });
  });


});